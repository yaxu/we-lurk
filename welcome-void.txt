Welcome to (void)

Writing code is one of the most fundamental ways of changing your computing environment. It allows you to mould your environment and blend it into your mental space. This act of creating a computing environment can alter the hacker as much as it shapes the technology. 

The (void) mailing list is for the discussion of how technology affects the psyche and the soul. We don't evangelise any points of view and we don't take ourselves too seriously. (void) is meant for the sharing of everything from the deep philosophy of unix to idle musing about the net.

You can send messages to the list  via email, by emailing void@we.lurk.org. Replying to a message you receive from the group does the same.

You can also browse past discussion threads, and also reply to them and make new ones, on the 'hyperkitty' web interface:
  https://we.lurk.org/hyperkitty/list/void@we.lurk.org/

You can access your settings via 'postorius':
  https://we.lurk.org/postorius/lists/void.we.lurk.org/

Please note: To use either web interface, you may need to make a new login if you don't already have one. If so, you'll need to use the same email address that you are subscribed with.

Feel free to post a message introducing yourself!

+-+-+-+-+-+
Guidelines
~~~~~~~~~~~

o Serious discussion and lighthearted chatter are equally 
  as welcome.

o We aim to be inclusive rather than exclusive.

o That's it. Chill.

