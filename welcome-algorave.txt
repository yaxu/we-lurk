Welcome to the algorave forum, a group for divese discussion + sharing around the algorave community.

You can send messages to the forum via email, by emailing algorave@we.lurk.org. Replying to a message you receive from the group does the same.

You can also browse past discussion threads, and also reply to them and make new ones, on the 'hyperkitty' web interface:
  https://we.lurk.org/hyperkitty/list/algorave@we.lurk.org/

You can access your settings via postorius:
  https://we.lurk.org/postorius/lists/algorave.we.lurk.org/

Please note: To use either web interface, you may need to make a new login if you don't already have one.

You can do that using a social media account (e.g. google, twitter, facebook etc) here:
  https://we.lurk.org/accounts/login/?next=%2Fpostorius%2Flists%2Falgorave.we.lurk.org%2F
  
Or otherwise sign up here:
  https://we.lurk.org/accounts/signup/?next=/postorius/lists/algorave.we.lurk.org/

In either case the email address you use will need to match the email address that you are subscribed to this group with.

For the Algorave homepage see:
  http://algorave.com/

Want to run an algorave? See the guidelines here:
  https://github.com/Algorave/guidelines/blob/master/README.md

For more info see TOPLAP, the home of live coding:
  http://toplap.org/
  
For a comprehensive list of live coding culture + technologies, see this list:
  https://github.com/toplap/awesome-livecoding/blob/master/README.md
  
Feel free to post a message introducing yourself + your interest in algorave
